# Edge detection

This project is about contour detection, which we carry out with the help of the gradient

### Execution parameters


```sh
data/leaves.jpg # Entrance image
-filtre 3 # Chosen filter
-seuillage 1 # Chosen threshold
-parametreSeuillage 100 # Eventual threshold parameter
-affinage 1 # Boolean for refining
-fermeture 1 # Boolean for closure
-detection 1 # Chosen primitive detection
-parametreDetection 35 # Eventual detection threshold
-rayon 100 # Radius for circle detection
```

`-filtre` value : 
- 1 : Prewitt filter
- 2 : Sobel filter
- 3 : Kirsch filter

`-seuillage` value:
- 1 : Unic Thresholding
- 2 : Global Thresholding
- 3 : Local Thresholding
- 4 : Hysteresis Thresholding

`-parametreSeuillage` value:

- Threshold value for Unic Thresholding
- Don't need for Global Thresholding
- Neighborhood size for Local Thresholding
- Hysteresis coefficient for Hysteresis Thresholding

`-detection` value:

- 1 : Lines detection
- 2 : Segment limitation
- 3 : Circles detection

`-parametreDetection` value:

- Threshold value for all the Hough transforms

`-rayon` value:

- Radius parameter for circles detection