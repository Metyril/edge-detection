#ifndef __MIF27_CONSTANTES__
#define __MIF27_CONSTANTES__

// Treshold methods indexes
enum Seuillages {UNIQUE = 1, GLOBAL, LOCAL, HYSTERESIS};


// Hough detection primitives
enum Detection {LIGNE = 1, SEGMENT, CERCLE};


// Gradient directions indexes
enum Directions {EST = 1, NORD_EST, NORD, NORD_OUEST, OUEST, SUD_OUEST, SUD, SUD_EST};


// Gradient directions color code
const cv::Vec3b couleurs[9] = {
    cv::Vec3b(0, 0, 0),
    cv::Vec3b(1, 0, 0),
    cv::Vec3b(1, 1, 0),
    cv::Vec3b(0, 1, 0),
    cv::Vec3b(1, 0, 1),
    cv::Vec3b(1, 0, 0),
    cv::Vec3b(0, 1, 1),
    cv::Vec3b(0, 0, 1),
    cv::Vec3b(1, 0, 1)
};


// Convolution filters' indexes
enum Filtres {PREWITT = 1, SOBEL, KIRSCH};


// Prewitt filter
const float prewittE[9] = {
    -1/3.f, 0, 1/3.f,
    -1/3.f, 0, 1/3.f,
    -1/3.f, 0, 1/3.f
};

const float prewittN[9] = {
    -1/3.f, -1/3.f, -1/3.f,
    0, 0, 0,
    1/3.f, 1/3.f, 1/3.f
};


// Sobel filter
const float sobelE[9] = {
    -0.25, 0, 0.25,
    -0.5, 0, 0.5,
    -0.25, 0, 0.25
};

const float sobelN[9] = {
    -0.25, -0.5, -0.25,
    0, 0, 0,
    0.25, 0.5, 0.25
};


// Kirsch filter
const float kirschE[9] = {
    -0.2, -0.2, 1/3.f,
    -0.2, 0, 1/3.f,
    -0.2, -0.2, 1/3.f
};

const float kirschN[9] = {
    1/3.f, 1/3.f, 1/3.f,
    -0.2, 0, -0.2,
    -0.2, -0.2, -0.2
};

const float kirschNE[9] = {
    -0.2, 1/3.f, 1/3.f,
    -0.2, 0, 1/3.f,
    -0.2, -0.2, -0.2
};

const float kirschSE[9] = {
    -0.2, -0.2, -0.2,
    -0.2, 0, 1/3.f,
    -0.2, 1/3.f, 1/3.f
};


#endif
