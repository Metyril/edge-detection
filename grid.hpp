#ifndef __MIF27_GRID__
#define __MIF27_GRID__
#define _USE_MATH_DEFINES


#include <opencv2/opencv.hpp>
#include <cmath>
#include <algorithm>
#include <vector>
#include "constantes.hpp"


/**
 * Class used to process images.
 */
class Grid {
    private:
        // Picture width and height.
        unsigned int width;
        unsigned int height;


        // Arrays of pixels, grandient norms and gradient directions.
        unsigned char* pixels;
        unsigned char* amplitudes;
        unsigned char* amplitudesSave;
        unsigned char* angles;
        std::vector<unsigned int> extremitesIndex;


        // Size of member pixels == number of pixels in the image.
        unsigned int arraySize;


    public:
        /**
         * Constructor for OpenCV image.
         * @param image An OpenCV image with type CV_8UC1.
         */
        Grid(const cv::Mat& image);


        /**
         * Constructor for minimalist initialization.
         * @param width Width of the grid.
         * @param height Height of the grid.
         */
        Grid(const unsigned int width, const unsigned int height);


        /**
         * Destructor freeing arrays allocated on the heap.
         */
        ~Grid();


        /**
         * Get 1D index from 2D indexes.
         * @param row Horizontal index.
         * @param col Vertical index.
         * @return Index in 1D.
         */
        unsigned int indexFrom2D(const unsigned int row, const unsigned int col) const;


        /**
         * Save an image which contains the edge detection processing.
         * @return The resulting cv::Mat image.
         */
        cv::Mat saveImage() const;


        /**
         * Find edges in an image and post-process it.
         * @param methodeGradient Choose a filter to convolute with the image.
         * @param affineContours Choose if the edges should be refined or not.
         */
        void contours(unsigned int methodeGradient, bool affineContours, unsigned int methodeSeuillage, unsigned int parametreSeuillage, bool fermeture);


        /**
         * Apply compass filter on pixel of index (i,j).
         * @param i Horizontal index.
         * @param j Vertical index.
         * @param filterE Horizontal convolution matrix.
         * @param filterN Vertical convolution matrix.
         * @param filterNE 45 degrees convolution matrix. Null for bidirectional gradient.
         * @param filterSE -45 degrees convolution matrix. Null for bidirectional gradient.
         */
        void gradient(unsigned int i, unsigned int j,
            const float* filterE, const float* filterN, const float* filterNE = nullptr, const float* filterSE = nullptr);


        /**
         * Refined edges based on local maxima following the gradient direction computed.
         */
        void affinage();

        /**
         *  Find edges extremities.
         */
        void extremites();

        /**
         * Close edges from extremities.
         */
        void fermetureContours();

        /**
         * Find tangent pixels depending on gradient direction.
         * @param index Current pixel index.
         * @return Pixel candidates.
         */
        unsigned int* findCandidate(unsigned int index);

        /**
         * Find the best path two pixels beyond candidates.
         * @param candidats Current candidates of pixel.
         * @return New candidates.
         */
        unsigned int* findCandidateLvl2(unsigned int* candidats);

        /**
         * Recursive function to walk alongside edge to close.
         * @param candidats Current candidates.
         * @param i Circuit-breaker in case it takes too much time.
         */
        void prolongementContours(unsigned int* candidats, unsigned int i);

        cv::Mat getGradiants();

        void transformeeHoughLignes(cv::Mat& contoursImg, int seuil = 35);
        void transformeeHoughSegments(cv::Mat& contoursImg, int seuil = 35);
        void transformeeHoughCercles(cv::Mat& contoursImg, float rayon, int seuil = 35);
};

cv::Mat unicThresholding(const cv::Mat& gradiants, const cv::Mat& img, int threshold);
cv::Mat globalThresholding(const cv::Mat& gradiants, const cv::Mat& img);
cv::Mat localThresholding(const cv::Mat& gradiants, const cv::Mat& img, int thresholdDim);
cv::Mat hysterisisThresholding(const cv::Mat& gradiants, const cv::Mat& imgs, int coeff);

#endif
