#include <opencv2/opencv.hpp>
#include "grid.hpp"

int main(int argc, char** argv) {
    // Check image was passed in arguments
    if (argc < 2) {
        printf("Usage : %s <path/image_file.extension>\n", argv[0]);
        return -1;
    }

    // Construct base image
    cv::Mat image = cv::imread(argv[1], cv::IMREAD_GRAYSCALE);
    // Check image was correctly constructed
    if (!image.data) {
        printf("Unable to read image %s\n", argv[1]);
        return -1;
    }

    if (image.type() != CV_8UC1) {
        printf("Sorry, but the program is not working for the type of your image\n");
        std::cout << "For developpers : Image type is "
            << cv::typeToString(image.type())
            << " when loading" << std::endl;
    }

    int filtre = PREWITT;
    int seuillage = UNIQUE;
    int parametreSeuillage = 0;
    bool affinage = false;
    bool fermeture = false;
    int detection = 0;
    int parametreDetection = 0;
    int rayon = 0;

    for (int i = 0; i < argc; i++) {
        if (strcmp(argv[i], "-filtre") == 0) {
            filtre = atoi(argv[i + 1]);
        }

        if (strcmp(argv[i], "-seuillage") == 0) {
            seuillage = atoi(argv[i + 1]);
        }

        if (strcmp(argv[i], "-parametreSeuillage") == 0) {
            parametreSeuillage = atoi(argv[i + 1]);
        }

        if (strcmp(argv[i], "-affinage") == 0) {
            affinage = atoi(argv[i + 1]);
        }

        if (strcmp(argv[i], "-fermeture") == 0) {
            fermeture = atoi(argv[i + 1]);
        }

        if (strcmp(argv[i], "-detection") == 0) {
            detection = atoi(argv[i + 1]);
        }

        if (strcmp(argv[i], "-parametreDetection") == 0) {
            parametreDetection = atoi(argv[i + 1]);
        }

        if (strcmp(argv[i], "-rayon") == 0) {
            rayon = atoi(argv[i + 1]);
        }
    }


    // Create a grid from image
    Grid grid(image);
    grid.contours(filtre, affinage, seuillage, parametreSeuillage, fermeture);
    cv::Mat resultImage = grid.saveImage();

    if (detection == LIGNE) {
        grid.transformeeHoughLignes(resultImage, parametreDetection);
    } else if (detection == SEGMENT) {
        grid.transformeeHoughSegments(resultImage, parametreDetection);
    } else if (detection == CERCLE) {
        grid.transformeeHoughCercles(resultImage, rayon, parametreDetection);
    }

    // Save transformed image
    cv::imwrite("tmp/gradient.png", resultImage);

    // Creation of windows
    cv::String imageWindowName = "Base image";
    cv::String resultWindowName = "Result image";

    cv::namedWindow(imageWindowName, cv::WINDOW_AUTOSIZE);
    cv::namedWindow(resultWindowName, cv::WINDOW_AUTOSIZE);

    // Display images
    cv::imshow(imageWindowName, image);
    cv::imshow(resultWindowName, resultImage);

    // Finish program on user will
    cv::waitKey(0);
    cv::destroyAllWindows();

    return 0;
}
