#include "grid.hpp"


Grid::Grid(const cv::Mat& image) : width(image.size().width),
    height(image.size().height), arraySize(this->width * this->height) {
    //Allocate memory
    this->pixels = new unsigned char[this->arraySize];
    this->amplitudes = new unsigned char[this->arraySize];
    this->amplitudesSave = new unsigned char[this->arraySize];
    this->angles = new unsigned char[this->arraySize];

    for (unsigned int i = 0; i < height; i++) {
        for (unsigned int j = 0; j < width; j++) {
            this->pixels[indexFrom2D(i, j)] = image.at<unsigned char>(i, j);
        }
    }
}


Grid::Grid(const unsigned int width, const unsigned int height)
    : width(width), height(height), arraySize(width * height) {
    // Allocate memory
    this->pixels = new unsigned char[this->arraySize];
    this->amplitudes = new unsigned char[this->arraySize];
    this->amplitudesSave = new unsigned char[this->arraySize];
    this->angles = new unsigned char[this->arraySize];
}


Grid::~Grid() {
    // Free memory
    delete[] this->pixels;
    delete[] this->amplitudes;
    delete[] this->amplitudesSave;
    delete[] this->angles;
}


unsigned int Grid::indexFrom2D(const unsigned int row, const unsigned int col) const {
    return this->width * row + col;
}


cv::Mat Grid::saveImage() const {
    cv::Mat image(this->height, this->width, CV_8UC3);
    unsigned int indiceCouleur = 0;

    for (unsigned int i = 0; i < this->height; i++) {
        for (unsigned int j = 0; j < this->width; j++) {
            indiceCouleur = this->angles[indexFrom2D(i, j)];
            image.at<cv::Vec3b>(i, j) = this->amplitudes[indexFrom2D(i, j)] * couleurs[indiceCouleur];
        }
    }

    return image;
}


void Grid::contours(unsigned int methodeGradient, bool affineContours, unsigned int methodeSeuillage, unsigned int parametreSeuillage, bool fermeture) {
    for (unsigned int i = 1; i < this->height - 1; i++) {
        for (unsigned int j = 1; j < this->width - 1; j++) {
            if (methodeGradient == PREWITT) {
                gradient(i, j, prewittE, prewittN);
            } else if (methodeGradient == SOBEL) {
                gradient(i, j, sobelE, sobelN);
            } else if (methodeGradient == KIRSCH) {
                gradient(i, j, kirschE, kirschN, kirschNE, kirschSE);
            }
        }
    }
    
    for (unsigned int i = 0; i < this->arraySize; i++) {
        this->amplitudesSave[i] = this->amplitudes[i];
    }

    cv::Mat filterImage = getGradiants();
    cv::Mat thresholdImage = saveImage();
    cv::Mat resultImage;

    if (methodeSeuillage == UNIQUE) {
        resultImage = unicThresholding(filterImage, thresholdImage, parametreSeuillage);
    } else if (methodeSeuillage == GLOBAL) {
        resultImage = globalThresholding(filterImage, thresholdImage);
    } else if(methodeSeuillage == LOCAL) {
        resultImage = localThresholding(filterImage, thresholdImage, parametreSeuillage);
    } else if(methodeSeuillage == HYSTERESIS) {
        resultImage = hysterisisThresholding(filterImage, thresholdImage, parametreSeuillage);
    }

    if (methodeSeuillage > 0 && methodeSeuillage < 5) {
        for (unsigned int i = 0; i < this->height; i++) {
            for (unsigned int j = 0; j < this->width; j++) {
                this->amplitudes[indexFrom2D(i, j)] = std::max({
                    resultImage.at<cv::Vec3b>(i, j)[0],
                    resultImage.at<cv::Vec3b>(i, j)[1],
                    resultImage.at<cv::Vec3b>(i, j)[2],
                });
            }
        }
    }

    if (affineContours) {
        affinage();
    }

    if (fermeture) {
        fermetureContours();
    }
}


void Grid::gradient(unsigned int i, unsigned int j,
    const float* filterE, const float* filterN, const float* filterNE, const float* filterSE) {
    int dE = 0;
    int dN = 0;
    int dNE = 0;
    int dSE = 0;
    unsigned int convIndex = 0;

    for (unsigned int a = i - 1; a <= i + 1; a++) {
        for (unsigned int b = j - 1; b <= j + 1; b++) {
            convIndex = 3 * ((a + 1) - i) + ((b + 1) - j);
            dE += this->pixels[indexFrom2D(a, b)] * filterE[convIndex];
            dN += this->pixels[indexFrom2D(a, b)] * filterN[convIndex];

            if (filterNE != nullptr && filterSE != nullptr) {
                dNE += this->pixels[indexFrom2D(a, b)] * filterNE[convIndex];
                dSE += this->pixels[indexFrom2D(a, b)] * filterSE[convIndex];
            }
        }
    }

    unsigned char norme = std::round(std::max({ dE, dN, dNE, dSE, -dE, -dN, -dNE, -dSE }));
    this->amplitudes[indexFrom2D(i, j)] = norme;

    if (norme != 0) {
        if (norme == dE) {
            this->angles[indexFrom2D(i, j)] = EST;
        } else if (norme == dN) {
            this->angles[indexFrom2D(i, j)] = NORD;
        } else if (norme == dNE) {
            this->angles[indexFrom2D(i, j)] = NORD_EST;
        } else if (norme == dSE) {
            this->angles[indexFrom2D(i, j)] = SUD_EST;
        } else if (norme == -dE) {
            this->angles[indexFrom2D(i, j)] = OUEST;
        } else if (norme == -dN) {
            this->angles[indexFrom2D(i, j)] = SUD;
        } else if (norme == -dNE) {
            this->angles[indexFrom2D(i, j)] = SUD_OUEST;
        } else if (norme == -dSE) {
            this->angles[indexFrom2D(i, j)] = NORD_OUEST;
        }
    }
}


void Grid::affinage() {
    unsigned int direction = 0;
    unsigned char autreContour = 0;

    for (unsigned int i = 1; i < this->height - 1; i++) {
        for (unsigned int j = 1; j < this->width - 1; j++) {
            if (this->amplitudes[indexFrom2D(i, j)] != 0) {
                direction = this->angles[indexFrom2D(i, j)];

                switch (direction) {
                    case EST: autreContour = this->amplitudes[indexFrom2D(i, j + 1)]; break;
                    case NORD_EST: autreContour = this->amplitudes[indexFrom2D(i - 1, j + 1)]; break;
                    case NORD: autreContour = this->amplitudes[indexFrom2D(i - 1, j)]; break;
                    case NORD_OUEST: autreContour = this->amplitudes[indexFrom2D(i - 1, j - 1)]; break;
                    case OUEST: autreContour = this->amplitudes[indexFrom2D(i, j - 1)]; break;
                    case SUD_OUEST: autreContour = this->amplitudes[indexFrom2D(i + 1, j - 1)]; break;
                    case SUD: autreContour = this->amplitudes[indexFrom2D(i + 1, j)]; break;
                    case SUD_EST: autreContour = this->amplitudes[indexFrom2D(i + 1, j + 1)]; break;
                    default: break;
                }

                if (autreContour > this->amplitudes[indexFrom2D(i, j)]) {
                    this->amplitudes[indexFrom2D(i, j)] = 0;
                }
            }
        }
    }
}


void Grid::extremites() {
    int connexity;
    for (unsigned int i = 2; i < this->height - 2; i++) {
        for (unsigned int j = 2; j < this->width - 2; j++) {
            connexity = 0;

            for (unsigned int a = i - 1; a <= i + 1; a++) {
                for (unsigned int b = j - 1; b <= j + 1; b++) {
                    if (this->amplitudes[indexFrom2D(i, j)] != 0) {
                        if (this->amplitudes[indexFrom2D(a, b)] != 0) {
                            connexity++;
                        }
                    }
                }
            }

            if (connexity == 2) {
                this->extremitesIndex.push_back(indexFrom2D(i, j));
            }
        }
    }
}


unsigned int* Grid::findCandidate(unsigned int index) {
    static unsigned int candidats[3] = { 0, 0, 0 };
    unsigned char test = this->angles[index];

    switch (this->angles[index]) {
        case EST:
            candidats[0] = index - this->width - 1;
            candidats[1] = index - this->width;
            candidats[2] = index - this->width + 1;
            break;
        case NORD_EST:
            candidats[0] = index - this->width;
            candidats[1] = index - this->width - 1;
            candidats[2] = index - 1;
            break;
        case NORD:
            candidats[0] = index - this->width - 1;
            candidats[1] = index - 1;
            candidats[2] = index + this->width - 1;
            break;
        case NORD_OUEST:
            candidats[0] = index - 1;
            candidats[1] = index + this->width - 1;
            candidats[2] = index + this->width;
            break;
        case OUEST:
            candidats[0] = index + this->width - 1;
            candidats[1] = index + this->width;
            candidats[2] = index + this->width + 1;
            break;
        case SUD_OUEST:
            candidats[0] = index + this->width;
            candidats[1] = index + this->width + 1;
            candidats[2] = index + 1;
            break;
        case SUD:
            candidats[0] = index + this->width + 1;
            candidats[1] = index + 1;
            candidats[2] = index - this->width + 1;
            break;
        case SUD_EST:
            candidats[0] = index + 1;
            candidats[1] = index - this->width + 1;
            candidats[2] = index - this->width;
            break;
        default:
            return nullptr;
    };

    return candidats;
}

unsigned int* Grid::findCandidateLvl2(unsigned int* candidats) {
    unsigned int candidatGagnant;
    unsigned int prochainCandidats[3];
    unsigned int* candidatsLvl2;
    unsigned int norme;
    unsigned int maxNorme = 0;

    for (unsigned int i = 0; i < 3; i++) {
        candidatsLvl2 = findCandidate(candidats[i]);
        if (candidatsLvl2 == nullptr) {
            break;
        }

        for (unsigned int j = 0; j < 3; j++) {
            norme = this->amplitudesSave[candidats[i]] + this->amplitudesSave[candidatsLvl2[j]];
            if (norme > maxNorme) {
                candidatGagnant = candidats[i];
                maxNorme = norme;
                for (unsigned int k = 0; k < 3; k++) {
                    prochainCandidats[k] = candidatsLvl2[k];
                }
            }
        }
    }

    if (candidatsLvl2 == nullptr) {
        return nullptr;
    }

    this->amplitudes[candidatGagnant] = 255;
    return prochainCandidats;
}


void Grid::fermetureContours() {
    extremites();

    for (unsigned int i = 0; i < this->extremitesIndex.size(); i ++) {
        unsigned int* candidats = findCandidate(this->extremitesIndex[i]);
        prolongementContours(candidats, 0);
    }
}


void Grid::prolongementContours(unsigned int* candidats, unsigned int i) {
    for (unsigned int i = 0; i < 3; i++) {
        if (std::find(this->extremitesIndex.begin(), this->extremitesIndex.end(), candidats[i]) != this->extremitesIndex.end()) {
            this->amplitudes[candidats[i]] = 255;
            return;
        }
    }

    if (i == 500) {
        return;
    }

    unsigned int* nouveauxCandidats = findCandidateLvl2(candidats);
    if (nouveauxCandidats == nullptr) {
        return;
    }
    prolongementContours(nouveauxCandidats, i + 1);
}


cv::Mat unicThresholding(const cv::Mat& gradiants, const cv::Mat& img, int threshold) {
    cv::Mat finalResult = img.clone();

    for (int i = 0; i < gradiants.rows; i++) {
        for (int j = 0; j < gradiants.cols; j++) {
            unsigned char gradiantValue = gradiants.at<unsigned char>(i, j);
            if (gradiantValue < threshold) {
                finalResult.at<cv::Vec3b>(i, j) = cv::Vec3b(0, 0, 0);
            }
            else {
                finalResult.at<cv::Vec3b>(i, j) = cv::Vec3b(255, 255, 255);
            }
        }
    }

    return finalResult;
}

int gradMean(const cv::Mat& gradiants) {
    int sum = 0;

    for (int i = 0; i < gradiants.rows; i++) {
        for (int j = 0; j < gradiants.cols; j++) {
            unsigned char gradiantValue = gradiants.at<unsigned char>(i, j);
            sum += gradiantValue;
        }
    }

    return sum / (gradiants.rows * gradiants.cols);
}

cv::Mat globalThresholding(const cv::Mat& gradiants, const cv::Mat& img) {
    int threshold = gradMean(gradiants);
    return unicThresholding(gradiants, img, threshold);
}

cv::Mat splitGrad(const cv::Mat& gradiants, int i, int j, int thresholdDim) {
    int dimR = (thresholdDim * 2) + 1;
    int dimC = (thresholdDim * 2) + 1;
    int startIndexI = i - thresholdDim;
    int startIndexJ = j - thresholdDim;

    if (i - thresholdDim < 0) {
        dimR -= thresholdDim - i;
        startIndexI = 0;
    }
    if (i + thresholdDim >= gradiants.rows) {
        dimR -= i + 1 + thresholdDim - gradiants.rows;
    }
    if (j - thresholdDim < 0) {
        dimC -= thresholdDim - j;
        startIndexJ = 0;
    }
    if (j + thresholdDim >= gradiants.cols) {
        dimC -= j + 1 + thresholdDim - gradiants.cols;
    }

    cv::Mat gradPart(dimR, dimC, CV_8UC3);

    int indexI = startIndexI;
    for (int k = 0; k < dimR; k++) {
        int indexJ = startIndexJ;
        for (int l = 0; l < dimC; l++) {
            const unsigned char gradiantValue = gradiants.at<unsigned char>(indexI, indexJ);
            gradPart.at<unsigned char>(k, l) = gradiantValue;
            indexJ++;
        }
        indexI++;
    }

    return gradPart;
}

cv::Mat localThresholding(const cv::Mat& gradiants, const cv::Mat& img, int thresholdDim) {
    cv::Mat finalResult = img.clone();
    int globalMeanHalf = gradMean(gradiants) / 2;

    for (int i = 0; i < gradiants.rows; i++) {
        for (int j = 0; j < gradiants.cols; j++) {
            cv::Mat tempGrad = splitGrad(gradiants, i, j, thresholdDim);
            unsigned char localTreshold = gradMean(tempGrad);

            unsigned char gradiantValue = gradiants.at<unsigned char>(i, j);
            if (gradiantValue <= localTreshold || localTreshold < globalMeanHalf) {
                finalResult.at<cv::Vec3b>(i, j) = cv::Vec3b(0, 0, 0);
            }
            else {
                finalResult.at<cv::Vec3b>(i, j) = cv::Vec3b(255, 255, 255);
            }
        }
    }

    return finalResult;
}

int standDeviation(const cv::Mat& gradiants, int mean) {
    int sumV = 0;

    for (int i = 0; i < gradiants.rows; i++) {
        for (int j = 0; j < gradiants.cols; j++) {
            unsigned char gradVal = gradiants.at<unsigned char>(i, j);
            sumV += std::pow(gradVal - mean, 2);
        }
    }

    return (int)sqrt(sumV / (gradiants.rows * gradiants.cols));
}

cv::Mat hysterisisThresholding(const cv::Mat& gradiants, const cv::Mat& img, int thresholdCoeff) {
    int mean = gradMean(gradiants);
    int stDev = standDeviation(gradiants, mean);

    int lowThreshold = std::abs(mean - (stDev * thresholdCoeff));
    int upThreshold = mean + (stDev * thresholdCoeff);

    cv::Mat finalResult = img.clone();

    for (int i = 0; i < gradiants.rows; i++) {
        for (int j = 0; j < gradiants.cols; j++) {
            unsigned char gradVal = gradiants.at<unsigned char>(i, j);
            if (gradVal < lowThreshold) {
                finalResult.at<cv::Vec3b>(i, j).val[0] = 0;
                finalResult.at<cv::Vec3b>(i, j).val[1] = 0;
                finalResult.at<cv::Vec3b>(i, j).val[2] = 0;
            }
            else if (gradVal > upThreshold) {
                finalResult.at<cv::Vec3b>(i, j).val[0] = 255;
                finalResult.at<cv::Vec3b>(i, j).val[1] = 255;
                finalResult.at<cv::Vec3b>(i, j).val[2] = 255;
            }
            else {
                finalResult.at<cv::Vec3b>(i, j).val[0] = 127;
                finalResult.at<cv::Vec3b>(i, j).val[1] = 127;
                finalResult.at<cv::Vec3b>(i, j).val[2] = 127;
            }
        }
    }

    for (int i = 0; i < gradiants.rows; i++) {
        for (int j = 0; j < gradiants.cols; j++) {
            if (finalResult.at<cv::Vec3b>(i, j).val[0] == 127) {
                cv::Mat tempGrad = splitGrad(finalResult, i, j, 1);
                int localTreshold = gradMean(tempGrad);
                if (localTreshold != 0) {
                    finalResult.at<cv::Vec3b>(i, j).val[0] = 255;
                    finalResult.at<cv::Vec3b>(i, j).val[1] = 255;
                    finalResult.at<cv::Vec3b>(i, j).val[2] = 255;
                }
                else {
                    finalResult.at<cv::Vec3b>(i, j).val[0] = 0;
                    finalResult.at<cv::Vec3b>(i, j).val[1] = 0;
                    finalResult.at<cv::Vec3b>(i, j).val[2] = 0;
                }
            }
        }
    }

    return finalResult;
}

cv::Mat Grid::getGradiants() {
    cv::Mat gradiantMat(this->height, this->width, CV_8UC3);

    for (int i = 0; i < this->height; i++) {
        for (int j = 0; j < this->width; j++) {
            gradiantMat.at<unsigned char>(i, j) = this->amplitudes[indexFrom2D(i, j)];
        }
    }

    return gradiantMat;
}

void Grid::transformeeHoughLignes(cv::Mat& contoursImg, int seuil) {
    int nbDistances = 2 * (int)std::sqrt(std::pow(this->height, 2) + std::pow(this->width, 2) + 0.5);
    int nbDirections = 360;
    int size = nbDistances * nbDirections;

    double* accumulateur = new double[size];

    for (unsigned int i = 0; i < nbDistances; i++) {
        for (unsigned int j = 0; j < nbDirections; j++) {
            accumulateur[nbDirections * i + j] = 0.0;
        }
    }

    for (unsigned int i = 0; i < this->height; i++) {
        for (unsigned int j = 0; j < this->width; j++) {
            if (this->amplitudes[indexFrom2D(i, j)] != 0) {
                for (int theta = 0; theta < nbDirections; theta ++) {
                    int rho = j * std::cos(theta * M_PI / 180.0) + i * std::sin(theta * M_PI / 180.0);
                    unsigned int index = nbDirections * (rho + nbDistances / 2) + theta;
                    accumulateur[index] += this->amplitudesSave[indexFrom2D(i, j)] / 255.0;
                }
            }
        }
    }

    for (unsigned int i = 0; i < nbDistances; i++) {
        for (unsigned int j = 0; j < nbDirections; j++) {
            if (accumulateur[nbDirections * i + j] > seuil) {
                double a = std::cos(j * M_PI / 180.0);
                double b = std::sin(j * M_PI / 180.0);
                double x0 = a * (i - nbDistances / 2);
                double y0 = b * (i - nbDistances / 2);

                cv::Point pt1, pt2;
                pt1.x = cvRound(x0 + 5000 * (-b));
                pt1.y = cvRound(y0 + 5000 * (a));
                pt2.x = cvRound(x0 - 5000 * (-b));
                pt2.y = cvRound(y0 - 5000 * (a));

                cv::line(contoursImg, pt1, pt2, cv::Scalar(255, 255, 255), 1, cv::LINE_AA);
            }
        }
    }

    delete[] accumulateur;
}


void Grid::transformeeHoughSegments(cv::Mat& contoursImg, int seuil) {
    transformeeHoughLignes(contoursImg, seuil);

    for (unsigned int i = 0; i < this->height; i++) {
        for (unsigned int j = 0; j < this->width; j++) {
            if (this->amplitudes[indexFrom2D(i, j)] < 10) {
                contoursImg.at<cv::Vec3b>(i, j) = cv::Vec3b(0, 0, 0);
            }
        }
    }
}


void Grid::transformeeHoughCercles(cv::Mat& contoursImg, float rayon, int seuil) {
    int nbDirections = 360;
    int size = this->height * this->width;

    double* accumulateur = new double[size];

    for (unsigned int i = 0; i < this->height; i++) {
        for (unsigned int j = 0; j < this->width; j++) {
                accumulateur[indexFrom2D(i, j)] = 0.0;
        }
    }

    for (unsigned int i = 0; i < this->height; i++) {
        for (unsigned int j = 0; j < this->width; j++) {
            if (this->amplitudes[indexFrom2D(i, j)] != 0) {
                for (int theta = 0; theta < nbDirections; theta++) {
                    int a = j - rayon * std::cos(theta * M_PI / 180.0);
                    int b = i - rayon * std::sin(theta * M_PI / 180.0);

                    unsigned int index = this->width * b + a;
                    if (index >= 0 && index < size) {
                        accumulateur[index] += this->amplitudesSave[indexFrom2D(i, j)] / 255.0;
                    }
                }
            }
        }
    }

    for (unsigned int i = 0; i < this->height; i++) {
        for (unsigned int j = 0; j < this->width; j++) {
            if (accumulateur[indexFrom2D(i, j)] > seuil) {
                cv::Point pt;
                pt.x = j;
                pt.y = i;

                cv::circle(contoursImg, pt, rayon, cv::Scalar(255, 255, 255), 1, cv::LINE_AA);
            }
        }
    }

    delete[] accumulateur;
}
